package fr.popschool.valenciennes.showcase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = {
        "fr.popschool.valenciennes.showcase.model"
})
public class ShowcaseApplication {

    public static void main(String[] args)
    {
        SpringApplication.run(ShowcaseApplication.class, args);
    }
}

