package fr.popschool.valenciennes.showcase.enumeration;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_NOTEMPLOYE
}

