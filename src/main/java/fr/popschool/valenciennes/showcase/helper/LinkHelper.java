package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Project;

public interface LinkHelper {
    void addProject(Project p);

    void removeProject(Project p);
}
