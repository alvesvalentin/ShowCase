package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Project;

import java.util.List;
import java.util.Set;

public interface CategoryHelper {
    void addProject(Project p);

    void addProjects(Set<Project> projects);

    void removeProject(Project p);
}
