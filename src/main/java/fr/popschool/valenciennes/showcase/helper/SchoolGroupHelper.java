package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.model.Factory;

public interface SchoolGroupHelper {
    void addCourse(Course course);

    void removeCourse(Course course);

    void addFactory(Factory factory);

    void removeFactory(Factory factory);
}
