package fr.popschool.valenciennes.showcase.helper;

import fr.popschool.valenciennes.showcase.model.Factory;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;

import java.util.Set;

public interface CourseHelper {
    void addSchoolGroup(SchoolGroup sg);

    void addSchoolGroups(Set<SchoolGroup> schoolGroups);

    void removeSchoolGroup(SchoolGroup sg);

    void addFactory(Factory f);

    void addFactories(Set<Factory> factories);

    void removeFactory(Factory f);
}
