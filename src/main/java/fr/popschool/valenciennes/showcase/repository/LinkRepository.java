package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.Link;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LinkRepository extends JpaRepository<Link, Long> {
    public Optional<Link> findByLink(String link);
}
