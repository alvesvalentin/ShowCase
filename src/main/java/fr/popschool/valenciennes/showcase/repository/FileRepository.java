package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.MediaType;

import java.awt.*;
import java.util.List;
import java.util.Optional;

public interface FileRepository extends JpaRepository<File, Long> {
    public Optional<File> findByPath(String path);
    public List<File> findByType(MediaType type);
}
