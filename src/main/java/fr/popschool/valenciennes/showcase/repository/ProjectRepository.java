package fr.popschool.valenciennes.showcase.repository;
import fr.popschool.valenciennes.showcase.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.*;

public interface ProjectRepository extends JpaRepository<Project, Long> {

    @Query("SELECT p FROM Project p WHERE creator LIKE %?1%")
    public List<Project> findByCreator(String creator);
    @Query("SELECT p FROM Project p WHERE name LIKE %?1%")
    public List<Project> findByName(String name);

}
