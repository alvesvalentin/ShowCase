package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.enumeration.RoleName;
import fr.popschool.valenciennes.showcase.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
