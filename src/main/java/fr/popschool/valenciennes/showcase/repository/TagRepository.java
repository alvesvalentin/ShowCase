package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Optional;
import java.util.Set;

public interface TagRepository extends JpaRepository<Tag, Long> {

    @Query("SELECT t FROM Tag t WHERE name LIKE %?1%")
    public Set<Tag> findByName(String name);
}
