package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.SchoolGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface SchoolGroupRepository extends JpaRepository<SchoolGroup, Long> {

    public Optional<SchoolGroup> findByName(String name);
    public List<SchoolGroup> findByYear(Date year);
}
