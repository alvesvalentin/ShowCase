package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.Set;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    @Query("SELECT c FROM Category c WHERE name LIKE %?1%")
    public Set<Category> findByName(String name);
}
