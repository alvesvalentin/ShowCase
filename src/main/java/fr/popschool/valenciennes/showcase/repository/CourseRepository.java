package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {
    public Optional<Course> findByName(String name);
}
