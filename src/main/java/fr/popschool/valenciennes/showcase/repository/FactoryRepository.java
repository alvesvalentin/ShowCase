package fr.popschool.valenciennes.showcase.repository;

import fr.popschool.valenciennes.showcase.model.Factory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FactoryRepository extends JpaRepository<Factory, Long> {

    public Optional<Factory> findByName(String name);
    public Optional<Factory> findByCity(String city);

 }
