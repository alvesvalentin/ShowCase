package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.User;
import fr.popschool.valenciennes.showcase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UserController implements DefaultController<User>{
    @Autowired
    private UserService userService;

    @Override
    @PostMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public User create(@RequestBody User object) {
        return userService.save(object);
    }

    @Override
    @GetMapping(value = "/user/{id}")
    public User findById(@PathVariable("id") Long id) {
        return userService.findById(id).orElseThrow(()-> new ResourceNotFoundException("User", "Id", id));
    }

    @Override
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> findAll() {
        return userService.findAll();
    }

    @Override
    @PutMapping(value = "/user/{id}")
    public User update(@PathVariable("id") Long id, @RequestBody User object) {
        userService.findById(id).orElseThrow(()->new ResourceNotFoundException("User","Id", id));
        return userService.save(object);
    }

    @Override
    @DeleteMapping(value = "/user")
    public void delete(@RequestBody User object) {
        userService.delete(object);
    }


}
