package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.BadRequestException;
import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CourseController implements DefaultController<Course>{

    @Autowired
    private CourseService courseService;

    @Override
    @PostMapping(value = "/course", produces = MediaType.APPLICATION_JSON_VALUE)
    public Course create(@RequestBody Course object) {
        return courseService.save(object);
    }

    @Override
    @GetMapping(value = "/course/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Course findById(@PathVariable("id") Long id) {
        return courseService.findById(id).orElseThrow(()->new ResourceNotFoundException("Course","Id", id));
    }

    @Override
    @GetMapping(value = "/courses", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Course> findAll() {
        return courseService.findAll();
    }

    @Override
    @PutMapping(value = "/course/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Course update(@PathVariable("id") Long id, @RequestBody Course object) {
        courseService.findById(id).orElseThrow(()-> new ResourceNotFoundException("Course", "Id", id));
        return courseService.save(object);
    }

    @Override
    @DeleteMapping(value = "/course")
    public void delete(@RequestBody Course object) {
        courseService.delete(object);
    }

 /*   @GetMapping(value = "/courses/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<Course> findByName(@RequestParam(value = "name", required = false) String name){
        if (name!=null) {
            return courseService.findByName(name);
        }
        throw new BadRequestException("Param name is required");
    }*/
}

