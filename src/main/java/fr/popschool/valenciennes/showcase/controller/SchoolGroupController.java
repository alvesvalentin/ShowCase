package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;
import fr.popschool.valenciennes.showcase.service.SchoolGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SchoolGroupController implements DefaultController<SchoolGroup>{
    @Autowired
    private SchoolGroupService schoolGroupService;

    @Override
    @PostMapping(value = "/school_group", produces = MediaType.APPLICATION_JSON_VALUE)
    public SchoolGroup create(@RequestBody SchoolGroup object) {
        return schoolGroupService.save(object);
    }

    @Override
    @GetMapping(value = "/school_group/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SchoolGroup findById(@PathVariable("id") Long id) {
        return schoolGroupService.findById(id).orElseThrow(()-> new ResourceNotFoundException("SchoolGroup", "Id", id));
    }

    @Override
    @GetMapping(value = "/school_groups", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SchoolGroup> findAll() {
        return schoolGroupService.findAll();
    }

    @Override
    @PutMapping(value = "/school_group/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public SchoolGroup update(@PathVariable("id") Long id, @RequestBody SchoolGroup object) {
        schoolGroupService.findById(id).orElseThrow(()->new ResourceNotFoundException("SchoolGroup","Id",id));
        return schoolGroupService.save(object);
    }

    @Override
    @DeleteMapping(value = "/school_group")
    public void delete(@RequestBody SchoolGroup object) {
        schoolGroupService.delete(object);
    }

    /*@GetMapping(value = "/school_groups/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public*/

}
