package fr.popschool.valenciennes.showcase.controller;
import fr.popschool.valenciennes.showcase.exception.BadRequestException;
import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProjectController implements DefaultController<Project> {
    @Autowired
    private ProjectService projectService;

    @PostMapping(value = "/project", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public Project create(@RequestBody Project object) {
        return projectService.save(object);
    }

    @GetMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public Project findById(@PathVariable("id") Long id) {
        return projectService.findById(id).orElseThrow(() -> new ResourceNotFoundException("Project", "Id", id));
    }


    @GetMapping(value = "/projects" )
    @Override
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @PutMapping(value = "/project/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Project update(@PathVariable("id") Long id, @RequestBody Project object) {
        Project project = projectService.findById(id).orElseThrow(()->new ResourceNotFoundException("Project", "Id", id));
        return projectService.save(object);
    }


    @Override
    @DeleteMapping(value ="/project")
    public void delete(@RequestBody Project object) {
        projectService.delete(object);
    }

    @GetMapping(value = "/projects/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Project> findByNameOrCreator(@RequestParam(value = "name", required = false) String name, @RequestParam(value="creator", required= false) String creator){
        if (name!=null){
            return projectService.findByName(name);
        }
        if (creator!= null){
            return projectService.findByCreator(creator);
        }
        throw new BadRequestException("Param name or creator is required");
    }

}
