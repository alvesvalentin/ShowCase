package fr.popschool.valenciennes.showcase.controller;

import java.util.List;

public interface DefaultController<T> {
    T create(T object);
    public T findById(Long id);
    public List<T> findAll();
    public T update(Long id, T object);
    public void delete(T object);
}
