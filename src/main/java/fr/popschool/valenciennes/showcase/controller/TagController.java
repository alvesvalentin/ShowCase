package fr.popschool.valenciennes.showcase.controller;

import fr.popschool.valenciennes.showcase.exception.BadRequestException;
import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Tag;
import fr.popschool.valenciennes.showcase.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class TagController implements DefaultController<Tag> {

    @Autowired
    private TagService tagService;

    @Override
    @PostMapping(value = "/tag", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tag create(@RequestBody Tag object) {
        return tagService.save(object);
    }


    @Override
    @GetMapping(value = "/tag/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tag findById(@PathVariable("id") Long id) {
        return tagService.findById(id).orElseThrow(()-> new ResourceNotFoundException("Tag", "Id", id));
    }

    @GetMapping(value = "/tags", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public List<Tag> findAll() {
        return tagService.findAll();
    }

    @PutMapping(value = "/tag/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Override
    public Tag update(@PathVariable("id") Long id, @RequestBody Tag object) {
        Tag tag= tagService.findById(id).orElseThrow(()->new ResourceNotFoundException("Tag", "Id", id));
        return tagService.save(object);
    }

    @DeleteMapping(value = "/tag")
    @Override
    public void delete(@RequestBody Tag object) {
        tagService.delete(object);
    }

    @GetMapping (value = "/tags/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<Tag> findByName(@RequestParam(value = "name", required = false) String name){
        if (name!=null) {
            Set<Tag> tag = tagService.findByName(name);
            return tag;
        }
        throw new BadRequestException("Param name is required");
    }
}
