package fr.popschool.valenciennes.showcase.service;

import java.util.List;
import java.util.Optional;

public interface DefaultService<T> {
    Optional<T> findById(Long id);
    List<T> findAll();

    T save(T object);
    void delete (T object);
}
