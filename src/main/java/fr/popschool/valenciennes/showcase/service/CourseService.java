package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.Course;

import java.util.Optional;

public interface CourseService extends DefaultService<Course> {
    public Optional<Course> findByName(String name);
}
