package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.Factory;

import java.util.Optional;

public interface FactoryService extends DefaultService<Factory> {
    public Optional<Factory> findByName(String name);
    public Optional<Factory> findByCity(String city);

}
