package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Category;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.repository.CategoryRepository;
import fr.popschool.valenciennes.showcase.repository.ProjectRepository;
import fr.popschool.valenciennes.showcase.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Override
    public Optional<Category> findById(Long id) {
        return categoryRepository.findById(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    @Override
    public Category save(Category object) {
        Set<Project> projects = new HashSet();
        for (Project p :
                object.getProjects()) {
            projects.add(projectRepository.findById(p.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", p.getId())));
        }
        object.setProjects(new HashSet<>());
        object.addProjects(projects);
        return categoryRepository.save(object);
    }

    @Override
    public void delete(Category object) {
        categoryRepository.delete(object);
    }

    @Override
    public Set<Category> findByName(String name) {

        return categoryRepository.findByName(name);
    }
}
