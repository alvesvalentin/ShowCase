package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.Link;

import java.util.Optional;

public interface LinkService extends DefaultService<Link> {
    public Optional<Link> findByLink(String link);
}
