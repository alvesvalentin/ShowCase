package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.model.Factory;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;
import fr.popschool.valenciennes.showcase.repository.CourseRepository;
import fr.popschool.valenciennes.showcase.repository.FactoryRepository;
import fr.popschool.valenciennes.showcase.repository.SchoolGroupRepository;
import fr.popschool.valenciennes.showcase.service.FactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class FactoryServiceImpl implements FactoryService {
    @Autowired
    FactoryRepository factoryRepository;
    @Autowired
    SchoolGroupRepository schoolGroupRepository;
    @Autowired
    CourseRepository courseRepository;

    @Override
    public Optional<Factory> findByName(String name) {
        return factoryRepository.findByName(name);
    }

    @Override
    public Optional<Factory> findByCity(String city) {
        return factoryRepository.findByCity(city);
    }

    @Override
    public Optional<Factory> findById(Long id) {
        return factoryRepository.findById(id);
    }

    @Override
    public List<Factory> findAll() {
        return factoryRepository.findAll();
    }

    @Override
    public Factory save(Factory object) {
        Set<SchoolGroup> schoolGroups = new HashSet<>();
        for (SchoolGroup sg : object.getSchoolGroup()){
            if(sg.getId() == null){
                schoolGroups.add(schoolGroupRepository.save(sg));
            }else {
                schoolGroups.add(schoolGroupRepository.findById(sg.getId()).orElseThrow(() -> new ResourceNotFoundException("SchoolGroup", "id", sg.getId())));
            }
        }
        object.setSchoolGroup(new HashSet<>());
        object.addSchoolGroups(schoolGroups);

        Set<Course> courses = new HashSet<>();
        for (Course c: object.getCourses()){
            if(c.getId() == null){
                courses.add(courseRepository.save(c));
            }else {
                courses.add(courseRepository.findById(c.getId()).orElseThrow(() -> new ResourceNotFoundException("Course", "id", c.getId())));
            }
        }
        object.setCourses(new HashSet<>());
        object.addCourses(courses);


        return factoryRepository.save(object);
    }

    @Override
    public void delete(Factory object) {
        factoryRepository.delete(object);
    }
}
