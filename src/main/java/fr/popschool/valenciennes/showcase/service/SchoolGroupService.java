package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.controller.DefaultController;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface SchoolGroupService extends DefaultService<SchoolGroup> {

    public Optional<SchoolGroup> findByName(String name);
    public List<SchoolGroup> findByYear(Date year);
}
