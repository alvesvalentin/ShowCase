package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.File;
import org.springframework.http.MediaType;

import java.util.List;
import java.util.Optional;

public interface FileService extends DefaultService<File> {
    public Optional<File> findByPath(String path);
    public List<File> findByType(MediaType type);
}
