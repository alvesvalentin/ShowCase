package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.*;
import fr.popschool.valenciennes.showcase.repository.*;
import fr.popschool.valenciennes.showcase.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LinkRepository linkRepository;

    @Autowired
    private FileRepository fileRepository;

    @Override
    public List<Project> findByCreator(String creator) {
        return projectRepository.findByCreator(creator);
    }

    @Override
    public List<Project> findByName(String name) {
        return projectRepository.findByName(name);
    }

    @Override
    public Optional<Project> findById(Long id) {
        return projectRepository.findById(id);
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project save(Project object) {

        Set<User> users = new HashSet();
        for (User us :
                object.getUsers()) {
            users.add(userRepository.findById(us.getId()).orElseThrow(() -> new ResourceNotFoundException("User", "id", us.getId())));
        }
        object.setUsers(new HashSet<>());
        object.addUsers(users);

        Set<Tag> tags = new HashSet<>();
        for (Tag tag : object.getTags()){
            if(tag.getId() == null){
                tags.add(tagRepository.save(tag));
            }else{
                tags.add(tagRepository.findById(tag.getId()).orElseThrow(() -> new ResourceNotFoundException("Tag", "id", tag.getId())));
            }
        }
        object.setTags(new HashSet<>());
        object.addTags(tags);

        Set<Category> categories = new HashSet<>();
        for (Category category : object.getCategories()){
            if(category.getId() == null){
                categories.add(categoryRepository.save(category));
            }else {
                categories.add(categoryRepository.findById(category.getId()).orElseThrow(() -> new ResourceNotFoundException("Category", "id", category.getId())));
            }
        }
        object.setCategories(new HashSet<>());
        object.addCategories(categories);

        Set<Link> links = new HashSet<>();
        for (Link link : object.getLinks()){
            if(link.getId() == null){
                links.add(linkRepository.save(link));
            }else {
                links.add(linkRepository.findById(link.getId()).orElseThrow(() -> new ResourceNotFoundException("Link", "id", link.getId())));
            }
        }
        object.setLinks(new HashSet<>());
        object.addLinks(links);

        Set<File> files = new HashSet<>();
        for (File file : object.getFiles()){
            if(file.getId() == null){
                files.add(fileRepository.save(file));
            }else {
                files.add(fileRepository.findById(file.getId()).orElseThrow(() -> new ResourceNotFoundException("File", "id", file.getId())));
            }
        }
        object.setFiles(new HashSet<>());
        object.addFiles(files);

        return projectRepository.save(object);

    }

    @Override
    public void delete(Project object) {
        projectRepository.delete(object);
    }
}
