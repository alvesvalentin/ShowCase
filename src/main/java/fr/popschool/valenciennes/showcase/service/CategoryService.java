package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.Category;
import org.springframework.scheduling.concurrent.ScheduledExecutorTask;

import java.util.Optional;
import java.util.Set;

public interface CategoryService extends DefaultService<Category> {
    public Set<Category> findByName(String name);
}
