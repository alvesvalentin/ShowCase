package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.model.Tag;
import fr.popschool.valenciennes.showcase.model.User;
import fr.popschool.valenciennes.showcase.repository.ProjectRepository;
import fr.popschool.valenciennes.showcase.repository.TagRepository;
import fr.popschool.valenciennes.showcase.repository.UserRepository;
import fr.popschool.valenciennes.showcase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    TagRepository tagRepository;
    @Autowired
    ProjectRepository projectRepository;


    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public Optional<User> findById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User save(User object) {

        Set<Project> projects = new HashSet<>();
        for (Project p : object.getProjects()){
            if(p.getId() == null){
                projects.add(projectRepository.save(p));
            }else{
                projects.add(projectRepository.findById(p.getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", p.getId())));
            }
        }
        object.setProjects(new HashSet<>());
        object.addProjects(projects);

        Set<Tag> tags = new HashSet<>();
        for (Tag tag : object.getTags()){
            if(tag.getId() == null){
                tags.add(tagRepository.save(tag));
            }else{
                tags.add(tagRepository.findById(tag.getId()).orElseThrow(() -> new ResourceNotFoundException("Tag", "id", tag.getId())));
            }
        }
        object.setTags(new HashSet<>());
        object.addTags(tags);

        return userRepository.save(object);
    }

    @Override
    public void delete(User object) {
        userRepository.delete(object);
    }
    @Override
    public Boolean existsByLogin(String login) {
        return userRepository.existsByLogin(login);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }



}
