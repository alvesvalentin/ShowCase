package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.Category;
import fr.popschool.valenciennes.showcase.model.Course;
import fr.popschool.valenciennes.showcase.model.Factory;
import fr.popschool.valenciennes.showcase.model.SchoolGroup;
import fr.popschool.valenciennes.showcase.repository.CategoryRepository;
import fr.popschool.valenciennes.showcase.repository.FactoryRepository;
import fr.popschool.valenciennes.showcase.repository.SchoolGroupRepository;
import fr.popschool.valenciennes.showcase.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    CourseService courseService;

    @Autowired
    SchoolGroupRepository schoolGroupRepository;

    @Autowired
    FactoryRepository factoryRepository;

    @Override
    public Optional<Course> findByName(String name) {
        return courseService.findByName(name);
    }

    @Override
    public Optional<Course> findById(Long id) {
        return courseService.findById(id);
    }

    @Override
    public List<Course> findAll() {
        return courseService.findAll();
    }

    @Override
    public Course save(Course object) {

        Set<Factory> factories = new HashSet<>();
        for (Factory factory : object.getFactories()){
            if(factory.getId() == null){
                factories.add(factoryRepository.save(factory));
            }else {
                factories.add(factoryRepository.findById(factory.getId()).orElseThrow(() -> new ResourceNotFoundException("Factory", "id", factory.getId())));
            }
        }
        object.setFactories(new HashSet<>());
        object.addFactories(factories);

        Set<SchoolGroup> schoolGroups = new HashSet<>();
        for (SchoolGroup sg : object.getSchoolGroups()){
            if(sg.getId() == null){
                schoolGroups.add(schoolGroupRepository.save(sg));
            }else {
                schoolGroups.add(schoolGroupRepository.findById(sg.getId()).orElseThrow(() -> new ResourceNotFoundException("SchoolGroup", "id", sg.getId())));
            }
        }
        object.setSchoolGroups(new HashSet<>());
        object.addSchoolGroups(schoolGroups);


        return courseService.save(object);
    }

    @Override
    public void delete(Course object) {
        courseService.delete(object);
    }
}
