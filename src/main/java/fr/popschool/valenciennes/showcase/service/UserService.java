package fr.popschool.valenciennes.showcase.service;

import fr.popschool.valenciennes.showcase.model.User;

import java.util.Optional;

public interface UserService extends DefaultService<User> {
    public Optional<User> findByEmail(String email);
    Boolean existsByLogin(String login);

    Boolean existsByEmail(String email);

}
