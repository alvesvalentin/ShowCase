package fr.popschool.valenciennes.showcase.service.impl;

import fr.popschool.valenciennes.showcase.exception.ResourceNotFoundException;
import fr.popschool.valenciennes.showcase.model.File;
import fr.popschool.valenciennes.showcase.model.Project;
import fr.popschool.valenciennes.showcase.repository.FileRepository;
import fr.popschool.valenciennes.showcase.repository.ProjectRepository;
import fr.popschool.valenciennes.showcase.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class FileServiceImpl implements FileService {

    @Autowired
    FileRepository fileRepository;
    @Autowired
    ProjectRepository projectRepository;

    @Override
    public Optional<File> findByPath(String path) {
        return fileRepository.findByPath(path);
    }

    @Override
    public List<File> findByType(MediaType type) {
        return fileRepository.findByType(type);
    }

    @Override
    public Optional<File> findById(Long id) {
        return fileRepository.findById(id);
    }

    @Override
    public List<File> findAll() {
        return fileRepository.findAll();
    }

    @Override
    public File save(File object) {
        Project project= new Project();
        if(object.getProjects().getId() == null){
            object.setProjects(projectRepository.save(project));
        }else{
            project=projectRepository.findById(object.getProjects().getId()).orElseThrow(() -> new ResourceNotFoundException("Project", "id", object.getProjects().getId()));
        }

        object.setProjects(new Project());
        object.addProject(project);

        return fileRepository.save(object);
    }

    @Override
    public void delete(File object) {
        fileRepository.delete(object);
    }
}
