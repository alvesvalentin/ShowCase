package fr.popschool.valenciennes.showcase.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.SchoolGroupHelper;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "school_groups")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class SchoolGroup implements SchoolGroupHelper {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    //@Column
    private String name;
    //@Column
    private Date year;

    @ManyToOne
    @JoinColumn(name = "course_id")
    private Course course;


    @ManyToOne
    @JoinColumn(name="factory_id")
    private Factory factory;

    public SchoolGroup(String name, Date year) {
        this.name = name;
        this.year = year;
    }

    public SchoolGroup(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Factory getFactory() {
        return factory;
    }

    public void setFactory(Factory factory) {
        this.factory = factory;
    }

    @Override
    public void addCourse(Course course) {
        this.course=course;
        course.getSchoolGroups().add(this);
    }

    @Override
    public void removeCourse(Course course) {
        this.course=null;
        course.getSchoolGroups().remove(this);
    }

    @Override
    public void addFactory(Factory factory) {
        this.factory= factory;
        factory.getSchoolGroup().add(this);
    }

    @Override
    public void removeFactory(Factory factory) {
        this.factory=null;
        factory.getSchoolGroup().remove(this);
    }
}
