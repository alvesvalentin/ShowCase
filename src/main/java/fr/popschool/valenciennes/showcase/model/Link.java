package fr.popschool.valenciennes.showcase.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.LinkHelper;

import javax.persistence.*;

@Entity
@Table(name="links")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Link implements LinkHelper {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String link;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project projects;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Project getProjects() {
        return projects;
    }

    public void setProjects(Project projects) {
        this.projects = projects;
    }

    @Override
    public void addProject(Project p) {
        this.projects =p;
        p.getLinks().remove(this);
    }

    @Override
    public void removeProject(Project p) {
        this.projects =null;
        p.getLinks().remove(this);
    }
}
