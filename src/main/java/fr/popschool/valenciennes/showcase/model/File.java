package fr.popschool.valenciennes.showcase.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.FileHelper;
import org.springframework.http.MediaType;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="files")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class File implements FileHelper {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String path;

    private MediaType type;

    private Byte[] img;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project projects;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Project getProjects() {
        return projects;
    }

    public void setProjects(Project projects) {
        this.projects = projects;
    }

    @Override
    public void addProject(Project p) {
        this.projects = p;
        p.getFiles().add(this);
    }

    @Override
    public void removeProject(Project p) {
        this.projects =null;
        p.getFiles().remove(this);
    }

}
