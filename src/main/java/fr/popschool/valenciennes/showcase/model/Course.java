package fr.popschool.valenciennes.showcase.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.CourseHelper;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "courses")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Course implements CourseHelper {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;
    private String name;

    @OneToMany(mappedBy = "course")
    private Set<SchoolGroup> schoolGroups= new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(
            name="courses_factories",
            joinColumns = {@JoinColumn(name ="factory_id")},
            inverseJoinColumns = {@JoinColumn(name="course_id")}
            )
    private Set<Factory> factories= new HashSet<>();

    public Course(String name) {
        this.name = name;
    }

    public Course(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Set<Factory> getFactories() {
        return factories;
    }

    public void setFactories(Set<Factory> factories) {
        this.factories = factories;
    }

    public Set<SchoolGroup> getSchoolGroups() {
        return schoolGroups;
    }

    public void setSchoolGroups(Set<SchoolGroup> schoolGroups) {
        this.schoolGroups = schoolGroups;
    }

    @Override
    public void addSchoolGroup(SchoolGroup sg) {
        this.schoolGroups.add(sg);
        sg.setCourse(this);
    }

    @Override
    public void removeSchoolGroup(SchoolGroup sg) {
        this.schoolGroups.remove(sg);
        sg.setCourse(null);
    }

    @Override
    public void addFactory(Factory f) {
        this.factories.add(f);
        f.getCourses().add(this);
    }

    @Override
    public void removeFactory(Factory f) {
        this.factories.remove(f);
        f.getCourses().remove(this);
    }

    @Override
    public void addSchoolGroups(Set<SchoolGroup> schoolGroups) {
        this.schoolGroups.addAll(schoolGroups);
        for(SchoolGroup sc: schoolGroups){
            sc.setCourse(this);
        }
    }

    @Override
    public void addFactories(Set<Factory> factories) {
        this.factories.addAll(factories);
        for (Factory f: factories){
            f.getCourses().add(this);
        }
    }
}
