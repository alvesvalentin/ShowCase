package fr.popschool.valenciennes.showcase.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import fr.popschool.valenciennes.showcase.helper.TagHelper;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="tags")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Tag implements TagHelper {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @ManyToMany(mappedBy = "tags")
    private Set<Project> projects = new HashSet<>();

    @ManyToMany(mappedBy = "tags")
    private Set<User> users = new HashSet<>();


    public Tag(String name) {
        this.name = name;
    }

    public Tag(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public void addProject(Project p) {
        this.projects.add(p);
        p.getTags().add(this);
    }

    @Override
    public void removeProject(Project p) {
        this.projects.remove(p);
        p.getTags().remove(this);
    }

    @Override
    public void addUser(User user) {
        this.users.add(user);
        user.getTags().add(this);
    }

    @Override
    public void removeUser(User user) {
        this.users.remove(user);
        user.getTags().remove(this);
    }

    @Override
    public void addUsers(Set<User> users) {
        this.users.addAll(users);
        for (User u: users){
            u.getTags().add(this);
        }
    }

    @Override
    public void addProjects(Set<Project> projects) {
        this.projects.addAll(projects);
        for(Project p: projects){
            p.getTags().add(this);
        }
    }
}
